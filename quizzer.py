from glob import glob
from os import path, chdir
from ntpath import basename
from random import shuffle
import sys


def main():
    print "\nWelcome to quizzer!"
    chdir(path.dirname(path.realpath(__file__)))

    quiz = File(get_filename(glob("quizzes/*")))
    quiz.take_quiz(Config())

    print "\nThanks for using quizzer!"


def get_filename(quizzes):
    print "\nThe following files are in the quizzes directory:"
    for file in quizzes:
        print basename(file)
    file = input("\nWhat file would you like to quiz? ")
    if check_file("quizzes/" + file):
        return "quizzes/" + file
    else:
        print "\nI'm sorry, '" + file + \
            "' does not exist in the quizzes directory " \
            "or you do not have permission to read it."
        return get_filename(glob("quizzes/*"))


def check_file(file):
    try:  # Checks to make sure the file is acessible
        with open(file):
            pass
            return True
    except IOError as e:
        return False


def yes_or_no(query):
    while True:
        reply = input(query + " (y/n) ").lower()
        if reply in ["y", "yes"]:
            return True
        elif reply in ["n", "no"]:
            return False
        else:
            print "\nPlease answer 'y', 'yes', 'n', or 'no'"


def ask_questions(questions, new=False):
    new_questions = []

    if new:
        if yes_or_no("\nWould you like to save a copy of this quiz?"):
            filename = input(
                "\nWhat would you like to call this quiz? "
            )
            saved_file = open("quizzes/" + filename, "w")
            for question in questions:
                saved_file.write(question.line)
            saved_file.close()
            print "\nSaved quiz as", filename

    if len(questions) > 1:
        random = yes_or_no(
            "\nBy default, prompts are given "
            "in the order listed in the file. "
            "Would you like to receive prompts in a random order instead?"
        )
        if random:
            shuffle(questions)

    print "\nLet's quiz! Respond to each prompt with the correct answer:"
    for question in questions:
        answer = input(
            "\nPrompt: " + question.prompt + "\nYour answer: "
        )

        correct = False
        if answer in question.values:
            correct = True

        if correct:
            print "Correct!"
        else:
            correct = "Quiz answer(s): " + question.values_string() + \
                "\n\nYour answer didn't match exactly. Is it correct?"
            if not yes_or_no(correct):
                new_questions.append(question)

    if not new_questions:
        print "\nCongratulations, you got all the questions right!"

    if yes_or_no("\nWould you like to take this quiz again?"):
        quiz_again = ask_questions(questions)
        return None

    if new_questions:
        again = "\nWould you like to make a new quiz " \
            "for the questions you got wrong?"
        if yes_or_no(again):
            new_quiz = ask_questions(new_questions, True)
            return None

    if yes_or_no("\nWould you like to take another quiz?"):
        new_quiz = File(get_filename(glob("quizzes/*")))
        new_quiz.take_quiz(Config())


class Config:
    def __init__(self):
        self.question_divider = ";;"
        self.value_divider = ",,"

        if check_file("config.txt"):
            config_file = File("config.txt")
            for line in config_file.lines:
                if line.startswith("question divider="):
                    self.question_divider = line[17:]
                if line.startswith("value divider="):
                    self.value_divider = line[14:]


class File:
    def __init__(self, target_filename):
        self.target_filename = target_filename
        self.lines = (line.rstrip('\n') for line in open(self.target_filename))

    def take_quiz(self, config):
        self.questions = []
        for line in self.lines:
            if line.find(config.question_divider) != -1:
                self.questions.append(Question(line, config))
        ask_questions(self.questions)


class Question:
    def __init__(self, line, config):
        self.line = line

        self.divider_location = self.line.find(config.question_divider)
        self.prompt = self.line[:self.divider_location]

        self.values_area = self.line[
            self.divider_location + len(config.question_divider):
        ]
        self.values = []

        self.value_start = 0
        next_value = self.values_area.find(config.value_divider)
        if next_value != -1:
            while next_value != -1:
                self.values.append(
                    self.values_area[self.value_start:next_value]
                )
                self.value_start = next_value + len(config.value_divider)
                next_value = self.values_area.find(
                    config.value_divider, next_value + 1
                )
            self.values.append(self.values_area[self.value_start:])
        else:
            self.values.append(self.values_area)

    def values_string(self):
        values = ""
        for value in self.values:
            values += value + ", "
        return values[:-2]


try:  # Allows for input instead of raw_input in Python 2.x and 3.x
    input = raw_input
except NameError:
    pass

if __name__ == "__main__":
    main()
